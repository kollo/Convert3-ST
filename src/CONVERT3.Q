;######################################################################
;##   Aufl�sungskoverter f�r alle Aufl�sungen   (c) Markus Hoffmann  ##
;##   Version 3.01 1993, 3.00 1989, 1.00 1988                        ##
;######################################################################
;                                LETZTE BEARBEITUNG: 12.03.1995

VERSION=$301
XB_ID='MHC3'    ; XBRA-Kennung

        TEXT

; -------------------- Hauptprogramm ----------------------------------

Begin:  move.l  4(sp),a0      ;Startadresse der Basepage in a0
        move.l  #$100,d6      ; Basepagel�nge
        add.l   12(a0),d6     ; + TEXT
        add.l   20(a0),d6     ; + DATA
        add.l   28(a0),d6     ; + BSS = L�nge des Programms in d6
        movem.l d6/a0,-(sp)

MAIN:   lea     phy_screen(pc),a0   ; Psydo-screen
        move.l  a0,d0
        and.l   #$FFFF00,d0         ; auf 256-er Grenze bringen
        move.l  d0,(phy_screen_adr-phy_screen)(a0)  ; und Adresse merken
        clr     (conv_flg-phy_screen)(a0)

        move     #2,-(sp)     ; alte Physikalische Screenadr.
        trap     #14
        addq.l   #2,sp
        move.l   d0,normal_phy_screen_adr  ; sichern

        move     #4,-(sp)          ; Aufl�sung ermitteln
        trap     #14               ;
        addq.l   #2,sp             ; und
        lea      echt_aufl(pc),a0  ;
        MOVE     D0,(a0)           ; merken.
        MOVE     D0,(AUFL-echt_aufl)(a0)

        move     #-1,-(sp)
        move     #11,-(sp)    ; Shiftstatus ermitteln
        trap     #13
        addq.l   #4,sp
        btst     #2,d0        ; Control gedr�ckt, dann
        Bne      BREAK        ; Programm nicht installieren

        pea      systest(pc)  ; Superexec
        move     #38,-(sp)
        trap     #14
        addq.l   #6,sp
        tst.l    d0
        BMI      IST_SCHON    ; Converter schon installiert
        BNE      ERROR        ; Falsche Systemkonfiguration

        BSR      CREATE_TABELLE

        pea      (BEGIN-128)(pc)   ; BASEPAGE+128
        move     #26,-(sp)         ; SETDTA
        trap     #1
        addq.l   #6,sp

        move     #1,-(sp)
        pea      searchnam(pc)
        move     #78,-(sp)
        trap     #1                ; SF_FIRST
        addq.l   #8,sp
        tst      d0                ; Programm im
        bmi.s    errncon           ; aktuellen Verzeichnis nicht gefunden

auswert lea      go_mel(pc),a0     ; schonmal den Ausgabestring
        moveq    #0,d0
        move.b   (begin-128+30+7)(pc),d0 ; Nummer hinter 'CONVERT'
        move.b   d0,(solchar-go_mel)(a0)
        sub      #'0',d0
        bmi.s    goe               ; Ung�ltige Nummer/Zeichen
        cmp      #2,d0
        bgt.s    goe               ;              "
        move     d0,(aufl-go_mel)(a0) ; Aufl�sung setzen
        bsr      print             ; Jetzt Meldung ausgeben (a0)
        bra.s    goi

errncon:  move   #1,-(sp)
          pea    searchnam2(pc)
          move   #78,-(sp)
          trap   #1
          addq.l #8,sp
          tst    d0
          beq.s  auswert
          lea    errorncon(pc),a0
          bsr    print
          bsr    wartetaste
goe:      bsr.s  eingabe

goi     pea      install(pc)       ; Superexec
        move     #38,-(sp)
        trap     #14
        addq.l   #6,sp
        tst.l    d0
        BNE      ERROR

        move     aufl(pc),-(sp)     ; Aufloesung setzen
        move.l   #-1,-(sp)
        move.l   #-1,-(sp)
        move     #5,-(sp)
        trap     #14
        lea      12(sp),sp

        movem.l  (sp)+,d6/a0

        clr.w    -(sp)         ;KEEPTERM
        move.l   d6,-(sp)
        move.w   #$31,-(sp)
        trap     #1

;--------- Ende des Hauptprogramms ----------------------

eingabe:     lea      string(pc),a0
             BSR      PRINT
             BSR      WARTETASTE

             and.l    #$00ff,d0
             cmp      #'2',d0
             bne.s    gt1
             MOVE     #2,AUFL
gt1:         cmp      #'1',d0
             bne.s    gt2
             MOVE     #1,AUFL
gt2:         cmp      #'0',d0
             bne.s    gt3
             MOVE     #0,AUFL
gt3:         cmp      #'?',d0
             bne.s    gt4
ANLEITUNG:   lea   anleitungs(pc),a0  ; Anleitung ausgeben
             BSR   PRINT
             BSR   WARTETASTE
             bra.s eingabe
gt4:         move  aufl(pc),d0
             CMP   ECHT_AUFL,d0    ; keine �nderung der Aufl�sung ?
             BNE.s  RET2
             lea   frage(pc),a0        ; dann nachfragen
             BSR     PRINT
             BSR     WARTETASTE
             and.l #$FF,d0
             cmp   #'J',d0
             BEQ.s   RET2
             cmp   #'j',d0
             BEQ.s   RET2

BREAK        clr   -(sp)          ; Ende ohne Installation
             trap  #1

Ist_schon:   lea   errortxt1(pc),a0
             BSR     PRINT
             BSR     WARTETASTE
             and.l    #$00ff,d0
             SUB     #'0',D0
             and.l    #3,d0
             move     d0,-(sp)
             move.l   #-1,-(sp)
             move.l   #-1,-(sp)
             move     #5,-(sp)
             trap     #14
             clr      -(sp)
             trap     #1

; Ende der Hauptroutine

; Pr�ft, ob der Konverter schonmal installiert wurde
systest:      move.l   $B8.s,a0
              move.l   n12(pc),d0
              CMP.L    (N12-XBIOS)(A0),d0
              bne.s    al1
              moveq.l  #-1,d0
ret2:         rts

; Beginnt die Xbios-Routine mit LEA ?
al1:          cmp      #$41fa,(a0)
              beq.s    \weiter
              lea      errortxt2(pc),a0
              move.l   a0,d0
              rts

; Ermittele Tabelle der Xbiosroutinen und suche nach JSR in Xbios(5)
\weiter       moveq.l  #0,d0
              move     2(a0),d0
              add.l    d0,a0
              move.l   2+2+4*5(a0),a0   ; adresse der Xbios(5)
\test_sl      cmp      #$4EB9,(a0)      ; Nach JSR  suchen
              beq.s    \weiter2         ; gefunden
              cmp      #$4e75,(a0)+     ; Schon auf Rts ?
              bne.s    \test_sl
              lea      errortxt2(pc),a0
              move.l   a0,d0
              rts

; Tr�gt die Adresse, die auf JSR folgt ein.
\weiter2      move.l   2(a0),auflkonf+2 ; Adresse der Routine
              moveq.l  #0,d0
              rts

install:  lea      install(pc),a0
          move.l   $B8,(weiter+2-install)(a0) ; Neuen XbiosHandler installieren
          move.l   #xbios,$b8
          move.l   $70,(old_vbl_int+2-install)(a0)  ; VBL_INTERRUPT Routine
          move.l   #vbl_int,$70       ; umleiten auf eigene
          move.l   $456,a0
\n_slot   addq.l   #4,a0              ; VBL-Routine in Vbl-Queue
          tst.l    (a0)               ; eintragen
          bne.s    \n_slot
          move.l   (a0),old_vbl
          move.l   #VBL,(a0)
          move.l   $46e.s,old_swv
          move.l   #swv,$46e.s        ; Routine bei Monitorwechsel
          moveq.l  #0,d0
          rts

CREATE_TABELLE:      ; Tabelle fuer Farbconvertierung 0 --> Mono
      LEA    TABELLE(pc),A2
      moveq  #0,d0
      moveq  #0,d1
      MOVE   #255,D5
\next moveq  #7,d4
\b_t  move   d4,d7
      add    d7,d7
      btst   d4,d0
      beq.s  \clr
      bset   d7,d1
      bra.s  \go
\clr  bclr   d7,d1
\go   DBF    D4,\B_T
      move   d1,(a2)+
      addq   #1,d0
      DBF    D5,\next
      rts

PRINT:  move.l  a0,-(sp)
        move    #9,-(sp)
        trap    #1
        addq.l  #6,sp
        rts
WARTETASTE: move    #1,-(sp)
            trap    #1
            addq.l  #2,sp
            rts

ERROR:     move.l  d0,a0
           bsr.s   print
           lea     errortxt(pc),a0
           bsr.s   print
           bsr.s   wartetaste
           clr     -(sp)      ; TERM
           trap    #1

           dc.l    'XBRA',XB_ID,0
VBL_INT:   subq    #1,$452.s    ; VBL gesperrt ?
           BMI     ENDE_V_INT   ; JA, DANN ENDE
           movem.l d0-a6,-(sp)
           move.b  $ffff8260,d0
           and.l   #3,d0
           cmp     echt_aufl(pc),d0  ; Aufl�sung ge�ndert ?
           beq.s   OK1           ; nein, -->
           cmp     #2,echt_aufl
           bne.s   doch_Nicht
DOCH_DOCH  move    d0,aufl       ; Aufl�sung nur konvertieren
           move.b  (echt_aufl+1)(pc),$ffff8260
           bra.s   OK1
doch_nicht cmp     #2,d0
           beq.s   doch_doch
           move    d0,aufl       ; Aufl�sung gesetzt lassen
           move    d0,echt_aufl
OK1        move.b  $ffff8201,d0
           move.b  $ffff8203,d1
           TST     CONV_FLG
           bne.s   conv_on
           cmp.b      (normal_phy_screen_adr+1)(pc),d0
           bne.s      ge�ndert1
           cmp.b      (normal_phy_screen_adr+2)(PC),d1
           bne.s      ge�ndert1
           bra.s      ok2
conv_on:   cmp.b      (phy_screen_adr+1)(pc),d0
           bne.s      ge�ndert2
           cmp.b      (phy_screen_adr+2)(pc),d1
           bne.s      ge�ndert2
           bra.s      ok2
ge�ndert1: move.b     $ffff8201,normal_phy_screen_adr+1
           move.b     $ffff8203,normal_phy_screen_adr+2
           bra.s      ok2
ge�ndert2: move.b     $ffff8201,normal_phy_screen_adr+1
           move.b     $ffff8203,normal_phy_screen_adr+2
           move.b     (phy_screen_adr+1)(pc),$ffff8201
           move.b     (phy_screen_adr+2)(pc),$ffff8203
OK2:       tst.l      $45e.s         ; neue Bildschirmsp-start-adr ?
           beq.s      no_b
           move.l     $45e.s,$44e.s
           move.l     $45e.s,d0
           BSR        NEW_PHYSICAL_SCREEN
           clr.l      $45e.s
no_B:
              movem.l (sp)+,d0-a6
ende_v_int:   addq   #1,$452.s
OLD_VBL_INT:  JMP $FC0634

         dc.l    'XBRA',XB_ID
old_vbl: dc.l    0
VBL:    tst     $43e.s
        bne.s   \ret
        addq    #1,count
        cmp     #8,count
        bne.s   \ret
        BSR     CONVERT
        CLR     COUNT
\ret    rts

        dc.l    'XBRA',XB_ID,0
XBIOS:  MOVE.L  $4a2.s,A1         ;DATENSICHERUNGSBEREICH
        move    (sp)+,d0          ;SR holen
        move    d0,-(a1)          ;und retten
        move.l  (sp)+,-(a1);      ;PC holen und retten
        movem.l d3-d7/a3-a7,-(a1) ;und Register retten
        move.l  a1,$4a2           ;Zeiger retten
        btst    #13,d0            ;Aufruf im Supervisor-Modus?
        bne.s   super             ;wenn so
        move.l  usp,sp            ;benutze USP
super   move    (sp)+,d0          ;hole Funktionsnummer
        cmp     #2,d0             ;vergleiche
        bne.s   n12
        sub.l   a5,a5
        bsr.s   xbios02
        bra.s   exit
N12:    cmp     #3,d0             ;vergleiche
        bne.s   n13
        sub.l   a5,a5
        bsr.s   xbios03
        bra.s   exit
N13:    cmp     #4,d0             ;vergleiche
        bne.s   n1
        sub.l   a5,a5
        bsr.s   xbios04
        bra.s   exit
n1      cmp     #5,d0             ;vergleiche
        bne.s   altes
        sub.l   a5,a5
        bsr.s   xbios05
exit    move.l  $4a2.s,a1           ;auf Sicherungsbereich
        movem.l (a1)+,d3-d7/a3-a7 ;Register zurueck
        move.l  (a1)+,-(sp)       ;PC wieder auf Stack
        move    (a1)+,-(sp)       ;SR dto.
        move.l  a1,$4a2.s           ;Zeiger zurueck
        rte
altes   move    d0,-(sp)
        move.l  $4a2.s,a1           ;auf Sicherungsbereich
        movem.l (a1)+,d3-d7/a3-a7 ;Register zurueck
        move.l  (a1)+,-(sp)       ;PC wieder auf Stack
        move    (a1)+,-(sp)       ;SR dto.
        move.l  a1,$4a2.s           ;Zeiger zurueck
weiter  jmp $FC0748
XBIOS02:     move.l normal_phy_screen_adr(pc),d0 ; physikalische B_adresse
             rts
XBIOS03:     move.l $44e.s,d0                    ; logische        "
             rts
XBIOS04:     move aufl(pc),d0                    ; Aufl�sung
             and.l #3,d0
             rts
XBIOS05:                         ;Phys. u. log. B.sp.adr. u. Aufl. setzen
             TST.L   4(A7)         ;Logbase neu?
             BMI.S   \phy           ;nein, soll bleiben, springe
             MOVE.L  4(A7),$44E(A5) ;ja, setze
\phy         TST.L   8(A7)          ;Physbase neu ?
             BMI.S   \aufloesung    ;nein -> springe
             move.l  8(sp),d0
             bsr.s   new_physical_screen
\aufloesung  TST.W   $C(A7)          ;Aufl. neu ?
             BMI.S   ende5           ;nein -> ende
             MOVE.B  $D(A7),$44C(A5) ;ja, setze in var
             move.b  $D(a7),aufl+1
             cmp     #2,echt_aufl   ; bei Mono-> Farbe: nicht in vedeoshift
             beq.s   sysin
             cmp.b   #2,$44c(a5)    ; bei Farbe-> Mono auch nicht
             beq.s   sysin
             moveq.l #0,d0
             move.b  $44c(a5),d0
             move    d0,echt_aufl
             MOVE.B  d0,-$7DA0       ; setze in Vedeoshifter
sysin        CLR.W   $452(A5)        ; Vblroutine sperren
AUFLKOnF     JSR     $FCA7C4         ; neue Aufl. konfigurieren
             MOVE    #1,$452.s       ; VBLROUTINE FREIGEBEN
ende5:       RTS


; Tr�gt je nach Konvertierung die neue Screenadresse ein.
; d0=screenadr.

NEW_PHYSICAL_SCREEN: 
           and.l         #$ffff00,d0
           move.l        d0,normal_phy_screen_adr
           TST           CONV_FLG     ; WIRD CONVERTIERT ?
           bne.s         ret          ; Ja, schon ende ....
           move.b        (normal_phy_screen_adr+1)(pc),$ffff8201.s
           move.b        (normal_phy_screen_adr+2)(pc),$ffff8203.s
ret        rts
           dc.l    'XBRA',XB_ID
old_swv:   dc.l    0
SWV:       Btst    #7,$FFfffa01.s
           beq.s   \MONOCH
           AND     #1,ECHT_AUFL
           bra.s   \weiter
\MONOCH    MOVE     #2,ECHT_AUFL
\WEITER    MOVE     #0,$44A.S
           MOVE.B  (aufl+1)(pc),$44C.s
           move.b  (echt_aufl+1)(pc),$ffff8260.s
           RTS

convert:   move    aufl(pc),d0
           cmp     echt_aufl(pc),d0
           bne.s   conv
           move.b  (normal_phy_screen_adr+1)(pc),$ffff8201.s
           move.b  (normal_phy_screen_adr+2)(pc),$ffff8203.s
           CLR     CONV_FLG
           rts

CONV       move.b  (phy_screen_adr+1)(pc),$ffff8201.s
           move.b  (phy_screen_adr+2)(pc),$ffff8203.s
           MOVE    #-1,CONV_FLG
           move.l  phy_screen_adr(pc),a0
           move.l  normal_phy_screen_adr(pc),a1
           cmp     #2,echt_aufl
           beq.s   farbconv
monoconv:  move    echt_aufl(pc),d2
           and.l   #3,d2
           cmp     aufl(pc),d2
           BEQ     L1_1    ; KEINE KONVERTIERUNG
           cmp     #1,d2
           beq.s   L1_2
           cmp     #0,d2
           beq.s   L0_2
           rts
farbconv:  move   aufl(pc),d2
           and.l  #3,d2
           cmp    #2,d2
           BEQ    L2_2     ; KEINE KONVERTIERUNG
           cmp    #1,d2
           BEQ    L2_1
           tst    d2
           BEQ.s  L2_0
           rts
L0_2:                               ; Farbe0 -> Mono :
            MOVE    #1,ECHT_AUFL    ; UMSCHALTEN AUF MITTLERE
            MOvE.B  #1,$ffFF8260
            rts

L1_2:       MOVE    #200-1,D4
scc21b:     moveq   #10-1,d5
            lea     80(a1),a2       ; Farbe1 -> Mono
SCC11b:     MOVE   (A1)+,(A0)+
            move   (a2)+,(a0)+
            MOVE   (A1)+,(A0)+
            move   (a2)+,(a0)+
            MOVE   (A1)+,(A0)+
            move   (a2)+,(a0)+
            MOVE   (A1)+,(A0)+
            move   (a2)+,(a0)+
            dbra    d5,scc11b
            lea     80(a1),a1
            dbra d4,scc21b
            rts

L2_0:      LEA     TABELLE(pc),A2
           MOVE    #200-1,D4       ; MOnOCHROM -> FARBE0:
scc10b:    moveq   #20-1,d5
SCC10:     clr     d0
           move.b  (a1)+,d0
           asl     #1,d0
           move    (a2,d0),d1
           lsl     #1,d1
           swap    d1
           clr     d0
           move.b  (a1)+,d0
           asl     #1,d0
           move    (a2,d0),d1
           lsl     #1,d1
           swap    d1
           clr     d0
           move.b  (a1)+,d0
           asl     #1,d0
           or      (a2,d0),d1
           swap    d1
           clr     d0
           move.b  (a1)+,d0
           asl     #1,d0
           or      (a2,d0),d1
           move.l  d1,80(a0)
           clr     d0
           move.b  (a1)+,d0
           asl     #1,d0
           move    (a2,d0),d1
           lsl     #1,d1
           swap    d1
           clr     d0
           move.b  (a1)+,d0
           asl     #1,d0
           move    (a2,d0),d1
           lsl     #1,d1
           swap    d1
           clr.w   d0
           move.b  (a1)+,d0
           asl     #1,d0
           or      (a2,d0),d1
           swap    d1
           clr     d0
           move.b  (a1)+,d0
           asl     #1,d0
           or      (a2,d0),d1
           move.l  d1,(a0)+

           dbra    d5,scc10
           lea     80(a0),a0
           dbra    d4,scc10b
           rts

L2_1:       MOVE   #200-1,D4
scc21:        lea    80(a0),a2
              moveq  #20-1,d5        ; Monochrom -> Farbe1:
SCC11:          MOVE   (A1)+,(A0)+
                move   (a1)+,(a2)+
                MOVE   (A1)+,(A0)+
                move   (a1)+,(a2)+
              dbra   d5,scc11
              lea    80(a0),a0
            dbra   d4,scc21
            rts
L1_1:
L2_2:      MOVE    #32000/8-1,D5  ; MONOCHROM -> MONO:
\hook      MOVE.L  (A1)+,(A0)+    ; nur kopieren (sollte eigentlich
           MOVE.L  (A1)+,(A0)+    ; nicht vorkommen)
           dbra    d5,\hook
           rts
 data
GO_MEL: dc.b 27,'l',27,'v CONVERT 101c (c) Markus Hoffmann RES='
solchar dc.b 'X',27,'q',13,10,0

STRING: dc.b 27,'E',27,'v',27,'p',9,'Aufl�sungsconverter  V.3.1c     (c) by Markus Hoffmann',9,9
        dc.b 10,13,10,27,'q','Programm zum Konvertieren aller Aufl�sungen '
        dc.b 10,13,  'f�r beide Monitore.',10
        dc.b 13,10,9,'Version 3: 1989-1993, Grundversion: 1988 ',13,10
        dc.b 13,10,10,27,'p Das Programm darf frei f�r den privaten und schulischen '
        dc.b 13,10,  'Gebrauch verwendet werden, gerwerbliche Rechte erwerben Sie gegen '
        dc.b 13,10,  'Einsendung von DM 20,- an den Autor !',27,'q'
        dc.b 13,10,10,'AUTOR:  Markus Hoffmann'
        dc.b 13,10,   '        Flotowstra�e 40'
        dc.b 13,10,   '        42289 Wuppertal',13,10,10
        dc.b 10,10,27,'p','Aufl�sung: [2|1|0|?] ',27,'q',0
        align

Frage:  dc.b 10,13,'CONVERT trotzdem installieren ? (J/N):',0
        align

ANLEITUNGS:dc.b 27,'E',27,'p',9,9,9,9,' A N L E I T U N G ',9,9,9,9,10,27,'q'
    dc.b 13,10,' Der Converter sollte aus dem AUTO-Ordner gestartet werden ;'
    dc.b 13,10,' Er l��t sich zwar auch so starten, aber hierbei gibt es evtl. Probleme mit'
    dc.b 13,10,' GEM, welches sich dann nicht richtig anpa�t und die 32000 Byte-Grenze des'
    dc.b 13,10,' Bildschirms �berschreitet. Das f�hrt mei�t zu einem Absturz oder zu Daten-'
    dc.b 13,10,' verlusten bei einigen RAM-Disks.'
    dc.b 13,10,' Dann geben Sie die gew�nschte Aufl�sung an. Relevant ist hier nur HIGH und'
    dc.b 13,10,' Farbe(1,0), da das Betriebsystem im Farbbetrieb die Aufl�sung je nach'
    dc.b 13,10,' DESKTOP.INF ver�ndert. Durch die Konvertierung dauert der Bildaufbau natur-'
    dc.b 13,10,' gem�� l�nger, daf�r bieten sich jetzt folgende Vorteile:'
    dc.b 13,10,'    Sie k�nnen jetzt alle Aufl�sungen mit einem(!) Monitor nutzen.'
    dc.b 13,10,'    Sie k�nnen jetzt eine Aufl�sung mit beiden Monitoren nutzen.'
    dc.b 13,10,' 1) Das Umschalten zwischen den Aufl�sungen kann mit XBIOS(5) oder durch'
    dc.b 13,10,'    erneutes Starten dieses Converters oder mit dem Desktop erfolgen.'
    dc.b 13,10,'    Nach dem Umschalten zwischen MONO und Farbe mu� GEM angepa�t'
    dc.b 13,10,'    werden. Dies gschieht durch Anw�hlen von VOREINSTELLUNG im Desktop und Be-'
    dc.b 13,10,'    st�tigen der Aufl�sung mit RETURN.'
    dc.b 13,10,'    (Es k�nnen hier die oben genannten Probleme mit GEM auftreten.)'
    dc.b 13,10,' 2) Sie k�nnen jetzt ohne weiteres den Monitor w�hrend des Betriebs wechseln,'
    dc.b 13,10,'    ohne da� der Rechner abst�rzt. Die Aufl�sung wird automatisch mit �bernommen.'
    dc.b 13,10,10,27,'p',' Bitte eine Taste dr�cken ...',27,'q',0

searchnam:  dc.b 'CONVERT?.PRG',0
searchnam2: dc.b '\AUTO\CONVERT?.PRG',0
Errorncon:dc.b       'Dieses Programm mu� CONVERTn.PRG hei�en,'
          dc.b 13,10,'mit n=0 LOW , n=1 MID , n=2 HIGH '
          dc.b 13,10,'wird automatisch installiert.'
          dc.b 13,10,'Ansonsten (n beliebig) werden die'
          dc.b 13,10,'Einstellungen nach Programmstart abge-'
          dc.b 13,10,'fragt.',13,10
          dc.b 13,10,' Taste = weiter',13,10,0

errortxt: dc.b 'CONVERT konnte nicht installiert werden !'
          dc.b 13,10,' Taste = Abbruch ...',10,13,0

errortxt1:dc.b ' Der Konverter ist schon installiert ! ',10,13
          dc.b ' Sie k�nnen jetzt die Aufl�sung �ndern.',13,10,10
          dc.b ' Bitte Aufl�sung angeben : ',0

errortxt2:dc.b    ' ---* !Falsche Systemkonfiguration! *---',13,10
          dc.b 10,' Entfernen Sie bitte alle residenten',13,10
          dc.b    ' Programme aus dem Speicher. CONVERT',13,10
          dc.b    ' mu� zuerst(!) installiert werden. Evtl.',13,10
          dc.b    ' m�ssen Sie die Reihenfolge im',13,10
          dc.b    ' AUTO-Ordner �ndern.',13,10,10,0
        align
   bss
echt_aufl:      ds.w 1
aufl:           ds.w 1
count:          ds.w 1
conv_flg:       ds.w 1
phy_screen_adr: ds.l 1
normal_phy_screen_adr: ds.l 1
tabelle:        ds.w 256
                ds.b 256     ; Platz zum Abrunden der Screenadresse
phy_screen:     ds.b 32000
        end
 